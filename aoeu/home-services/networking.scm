;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu home-services networking)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu services configuration)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (aoeu packages gnunet)
  #:export (home-gnunet-service-type
            home-gnunet-configuration))

;;; Config serialization

(define (serialize-gnunet-config field-name val)
  (define (kebab-case->screaming-snake-case symbol)
    (string-map
     (lambda (char) (if (char=? char #\-) #\_
                   (char-upcase char)))
     (symbol->string symbol)))
  (define (serialize-section-name name)
    (format #f "[~a]" name))
  (define (serialize-assignment name value)
    (format #f "~a = ~a"
            (kebab-case->screaming-snake-case name)
            value))
  (define (serialize-section section)
    (string-join
     (cons (serialize-section-name (car section))
           (map (lambda (assignment)
                  (apply serialize-assignment
                         assignment))
                (cdr section)))
     "\n"
     'suffix))
  #~(string-join
     '#$(map serialize-section val)
     "\n"))

;;; Config predicate
(define (gnunet-config? config)
  (and-map (lambda (section)
             (and (symbol? (car section))
                  (and-map
                   (lambda (assignment)
                     (and (list? assignment)
                          (= 2 (length assignment))))
                   (cdr section))))
           config))

(define (home-gnunet-shepherd-service _)
  (list
   (shepherd-service
    (provision '(gnunet-home))
    (documentation "Run GNUnet.")
    (start #~(make-system-constructor #$(file-append
                                         gnunet
                                         "/bin/gnunet-arm")
                                      " --start"))
    (stop #~(make-system-destructor #$(file-append
                                       gnunet
                                       "/bin/gnunet-arm")
                                    " --end")))))

(define-configuration home-gnunet-configuration
  (package
    (package gnunet)
    "The GNUnet package to use.")
  (config
   (gnunet-config '())
   "GNUnet config represented as lists."))

(define (add-gnunet-configuration config)
  `(("config/gnunet.conf"
     ,(mixed-text-file
       "gnunet-config"
       (serialize-configuration config home-gnunet-configuration-fields)))))

(define add-gnunet-package
  (compose list home-gnunet-configuration-package))

(define home-gnunet-service-type
  (service-type
   (name 'home-gnunet)
   (extensions
    (list (service-extension
           home-shepherd-service-type
           home-gnunet-shepherd-service)
          (service-extension
           home-profile-service-type
           add-gnunet-package)
          (service-extension
           home-files-service-type
           add-gnunet-configuration)))
   (default-value #f)
   (description "Run GNUnet.")))
