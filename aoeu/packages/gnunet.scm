;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages gnunet)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)     ; inetutils
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages gnunet)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages web)
  #:use-module (gnu packages libidn)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages libunistring)
  #:use-module (gnu packages upnp)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages vim)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu))

;; TODO: Write service
;; TODO: Fix tests (they exist, but I can't run them)
(define-public gnunet
  (package
    (name "gnunet")
    (version "0.16.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://gnu/gnunet/gnunet-" version
                           ".tar.gz"))
       (sha256
        (base32
         "0p0ww3w62wiinpv9j25v23rd1hw7ksrfc878ayy5wa0w1gfjd06z"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f))
    (inputs
     `(("bluez" ,bluez)
       ("glpk" ,glpk)
       ("gnurl" ,gnurl)
       ("gnutls" ,gnutls/dane)
       ("gstreamer" ,gstreamer)
       ("inetutils" ,inetutils)
       ("iptables" ,iptables)
       ("jansson" ,jansson)
       ("libextractor" ,libextractor)
       ("libidn" ,libidn2)
       ("libgcrypt" ,libgcrypt)
       ("libjpeg" ,libjpeg-turbo)
       ("libltdl" ,libltdl)
       ("libmicrohttpd" ,libmicrohttpd)
       ("libogg" ,libogg)
       ("libsodium" ,libsodium)
       ("libunistring" ,libunistring)
       ("opus" ,opus)
       ("pulseaudio" ,pulseaudio)
       ("sqlite" ,sqlite)
       ("zbar" ,zbar)
       ("zlib" ,zlib)))
    (native-inputs
     `(("curl" ,curl)
       ("pkg-config" ,pkg-config)
       ("python" ,python)
       ("xxd" ,xxd)
       ("which" ,(@ (gnu packages base) which))))
    (propagated-inputs
     `(("miniupnpc" ,miniupnpc)))
    (synopsis "Secure, decentralized, peer-to-peer networking framework")
    (description
     "GNUnet is a framework for secure peer-to-peer networking.  The
high-level goal is to provide a strong foundation of free software for a
global, distributed network that provides security and privacy.  GNUnet in
that sense aims to replace the current internet protocol stack.  Along with
an application for secure publication of files, it has grown to include all
kinds of basic applications for the foundation of a GNU internet.")
    (license license:agpl3+)
    (home-page "https://gnunet.org/en/")))

;; NOTE: This ‘FIXME’ is from original package definition
;; FIXME: "gnunet-setup" segfaults under certain conditions and "gnunet-gtk"
;; does not seem to be fully functional.  This has been reported upstream:
;; http://lists.gnu.org/archive/html/gnunet-developers/2016-02/msg00004.html
(define-public gnunet-gtk
  (package
    (name "gnunet-gtk")
    (version "0.15.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/gnunet/gnunet-gtk-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "07zfhm8yk4zx20qzdd5zrjx4ap494yrcfbqm6nlgy19vjjmfbchl"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       (list "--with-libunique"
             "--with-extractor"
             "--with-qrencode"
             "--with-gnutls"
             (string-append "--with-glade="
                            (assoc-ref %build-inputs "glade3"))
             (string-append "--with-gnunet="
                            (assoc-ref %build-inputs "gnunet")))))
    (inputs
     `(("glade3" ,glade3)
       ("gnunet" ,gnunet)
       ("libsodium" ,libsodium)
       ("gnutls" ,gnutls/dane)
       ("gtk+" ,gtk+)
       ("libextractor" ,libextractor)
       ("libgcrypt" ,libgcrypt)
       ("libunique" ,libunique)
       ("qrencode" ,qrencode)))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("libglade" ,libglade)))
    (synopsis "Graphical front-end tools for GNUnet")
    (description "GNUnet is a framework for secure peer-to-peer
networking.  The high-level goal is to provide a strong foundation of
free software for a global, distributed network that provides security
and privacy.  GNUnet in that sense aims to replace the current internet
protocol stack.  Along with an application for secure publication of
files, it has grown to include all kinds of basic applications for the
foundation of a GNU internet.")
    (license license:agpl3+)
    (home-page "https://gnunet.org/en/")))
