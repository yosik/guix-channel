;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages web-browsers)
  #:use-module (guix packages)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix git-download)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:))

;; TODO: Maybe try to split into QtWebKit and QtWebEngine version?
(define-public otter-browser
  (package
    (name "otter-browser")
    (version "1.0.02")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OtterBrowser/otter-browser/")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "1wyancrx1bijlica8xijihrf7zmv968q1j91n9gm8idk3pwchjl7"))
       (file-name (git-file-name name version))))
    (build-system qt-build-system)
    (inputs
     `(("qtbase" ,qtbase-5)
       ("qtmultimedia" ,qtmultimedia)
       ("qtdeclarative" ,qtdeclarative)
       ("qtsvg" ,qtsvg)
       ("qtxmlpatterns" ,qtxmlpatterns)
       ("qtwebchannel" ,qtwebchannel)
       ("qtwebengine" ,qtwebengine)
       ("qtwebkit" ,qtwebkit)))
    (arguments
     '(#:tests? #f
       #:build-type "Release"))
    (synopsis "Free web browser that aims to recreate aspects of Opera
12 using Qt")
    (description
     "Otter Browser aims to recreate the best aspects of Opera 12 and to
revive its spirit.  We are focused on providing the powerful features
power users want while keeping the browser fast and lightweight.")
    (license license:gpl3+)
    (home-page "https://otter-browser.org/")))

(define-public moenavigator-engine
  (package
    (name "moenavigator-engine")
    (version "master")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://codeberg.org/moenavigator/moenavigatorengine")
             (commit "master")))
       (sha256
        (base32
         "12vnfhjfpslfj26y8hdvhbb37d0cqp8g2qkmr16vfyq8h63lvdip"))
       (file-name (git-file-name name version))))
    (build-system cmake-build-system)
    (inputs
     `(("gnutls" ,gnutls)))
    (arguments
     '(#:tests? #f
       #:build-type "Release"))
    (synopsis "Web browser engine written from scratch in C++. The goal of this
project is to create a modular, fast and flexible web browser engine.")
    (description
     "")
    (license license:gpl3+)
    (home-page "https://codeberg.org/moenavigator/moenavigatorengine")))

(define-public falkon
  (package
    (name "falkon")
    (version "3.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://invent.kde.org/network/falkon")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "0c9gwsp7f6j4z82r0lhdrah4hdhjavdx8j0q1hlx8gkmcxhvvj3s"))
       (file-name (git-file-name name version))))
    (build-system qt-build-system)
    (inputs
     `(("qtbase" ,qtbase-5)
       ("qtdeclarative" ,qtdeclarative)
       ("qtx11extras" ,qtx11extras)
       ("qtwebchannel" ,qtwebchannel)
       ("qtwebengine" ,qtwebengine)
       ("karchive" ,karchive)
       ("xcb-util" ,xcb-util)
       ("openssl" ,openssl)))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (arguments
     '(#:tests? #f
       #:build-type "Release"))
    (synopsis "Cross-platform Qt-based web browser")
    (description
     "Falkon is a new and very fast Qt web browser. It aims to be a lightweight
web browser available through all major platforms. This project has been
originally started only for educational purposes. But from its start, Falkon has
grown into a feature-rich browser.")
    (license license:gpl3+)
    (home-page "https://www.falkon.org/")))
