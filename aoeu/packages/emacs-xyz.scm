;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages emacs-xyz)
  #:use-module (srfi srfi-1)
  #:use-module (guix packages)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module ((guix licenses) #:prefix license:))

(define-public emacs-plz
  (package
    (name "emacs-plz")
    (version "7e45663")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/alphapapa/plz.el")
             (commit "7e456638a651bab3a814e3ea81742dd917509cbb")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "05kgxrps1s20im5hhq799nrs3615bvssm4r0ysgmwm203mmzsjgj"))))
    (build-system emacs-build-system)
    (inputs
     `(("curl" ,curl)))
    (synopsis "An HTTP library for Emacs")
    (description "plz is an HTTP library for Emacs. It uses curl as a backend,
which avoids some of the issues with using Emacs’s built-in url library. It
supports both synchronous and asynchronous requests. Its API is intended to be
simple, natural, and expressive. Its code is intended to be simple and
well-organized. Every feature is tested against httpbin.")
    (license license:gpl3)
    (home-page "https://github.com/alphapapa/plz.el")))

(define-public emacs-ement
  (package
    (name "emacs-ement")
    (version "c951737")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/alphapapa/ement.el")
             (commit "c951737dc855604aba389166bb0e7366afadc533")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "00iwwz4hzg4g59wrb5df6snqz3ppvrsadhfp61w1pa8gvg2z9bvy"))))
    (build-system emacs-build-system)
    (propagated-inputs
     `(("emacs-plz" ,emacs-plz)
       ("emacs-ts" ,emacs-ts)))
    (synopsis "Matrix client for Emacs")
    (description "Ement.el is a new Matrix client for Emacs. It's basic at
the moment, but it can be used to send and read messages (including replies and
images), join and leave rooms, etc.")
    (license license:gpl3)
    (home-page "https://github.com/alphapapa/ement.el")))

(define-public emacs-project-x
  (package
    (name "emacs-project-x")
    (version "0b78f4e")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/karthink/project-x")
             (commit "0b78f4e33b994612fcb305b3cf6d3b1e3b62cea7")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1xxzxxm0jila5s9sfay6ywj2j8pyz4wwcrycvnrrzh0vxcsgzf9s"))))
    (build-system emacs-build-system)
    (synopsis "Enhancements to Emacs' built in project library.")
    (description "Project-X adds a couple of convenience features for Emacs’
project.el library.")
    (license license:gpl3+)
    (home-page "https://github.com/karthink/project-x")))

(define-public emacs-compile-multi
  (package
    (name "emacs-compile-multi")
    (version "f4c3157")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mohkale/compile-multi")
             (commit "f4c315734a095a4ca9b0d085ccd5556106008428")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ndbbb3m3zaji8b8v86wzvmpyrqgasdgsph7fhh0nxn6g91392n0"))))
    (build-system emacs-build-system)
    (propagated-inputs
     `(("emacs-s" ,emacs-s)))
    (synopsis "Multi target interface to compile.el")
    (description "This package provides a command that can be used to
run compilation commands that match one or more predicates for the
current directory or project. This can be used for example to list all
of the makefile targets of a project and then execute one.")
    (license license:gpl3+)
    (home-page "https://github.com/mohkale/compile-multi")))

(define-public emacs-vcomplete
  (package
    (name "emacs-vcomplete")
    (version "1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~dsemy/vcomplete/")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1a4k57m3av37wfb145irp253rclv7pjq28chpsbjx650ywib1ra6"))))
    (build-system emacs-build-system)
    (synopsis "Visual completions")
    (description
     "Vcomplete provides a minor mode enhancing the default completion
list buffer, providing visual aids for selecting completions.")
    (license license:gpl3+)
    (home-page "https://git.sr.ht/~dsemy/vcomplete/")))

(define-public emacs-elmo
  (package
    (name "emacs-elmo")
    (version "54ba443")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/karthink/elmo")
             (commit "54ba443fe91fe455478eecdb1a01a444a5177e84")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0p7z91vmfkchglj600v7mn7n20r5q15312ds7813yza7vdj7313q"))))
    (propagated-inputs (list emacs-embark))
    (build-system emacs-build-system)
    (synopsis "Embark Live MOde for Emacs")
    (description
     "This is a collection of opinionated customizations to use Embark
live buffers as my incremental completion and selection system.")
    (license license:gpl3+)
    (home-page "https://github.com/karthink/elmo")))

(define-public emacs-popper
  (package
    (name "emacs-popper")
    (version "7b02960")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/karthink/popper.git")
             (commit "7b02960025fb89384f78ba12ad03cae0ddf1e411")))
       (sha256
        (base32 "0p12zz2lhm10yikhnq52z66xwy64gcvig42bzajv5q7x09qvvna7"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/karthink/popper")
    (synopsis "Summon and dismiss buffers as popups")
    (description
     "Popper is a minor-mode to tame the flood of ephemeral windows Emacs
produces, while still keeping them within arm's reach. Designate any
buffer to \"popup\" status, and it will stay out of your way. Disimss or
summon it easily with one key. Cycle through all your \"popups\" or just
the ones relevant to your current buffer. Useful for many things,
including toggling display of REPLs, documentation, compilation or shell
output, etc.")
    (license license:gpl3+)))

(define-public emacs-vundo
  (package
    (name "emacs-vundo")
    (version "1da7578")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/casouri/vundo")
             (commit "1da75782e06a2febdd81c698d9ea15597e6102ba")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1kkkw3352zz4w41dxq8m0183i61d3ays4rfyny9aia2pf48sy8cw"))))
    (build-system emacs-build-system)
    (synopsis "Visualize the undo tree.")
    (description "vundo.el visualizes the undo tree in emacs.")
    (license license:gpl3+)
    (home-page "https://github.com/casouri/vundo")))

(define-public emacs-tempel
  (package
    (name "emacs-tempel")
    (version "5242ebb")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/minad/tempel")
             (commit "5242ebb37fbf14f60c8eecf4461091ee22389912")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1j6f9y50w4h7cn4bl3dabv8d1i94kc0cf0rla825lglai206cwrx"))))
    (build-system emacs-build-system)
    (synopsis "Simple templates for Emacs")
    (description "Tempel is a tiny template package for Emacs, which
uses the syntax of the Emacs Tempo library.")
    (license license:gpl3+)
    (home-page "https://github.com/minad/tempel")))

(define-public emacs-recomplete
  (let ((commit "d0d380929460ff35534900e34ababad43d23c966")
        (revision "1"))
    (package
      (name "emacs-recomplete")
      (version (git-version "0.2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/ideasman42/emacs-recomplete")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "178415wsvjvji4caz72mksrv4krr7aykh02cggnp41w66chm503y"))))
      (build-system emacs-build-system)
      (synopsis "Immediately (re)complete actions")
      (description "Immediate completion, without prompting.
Unlike most completion, recomplete performs the completion action,
calling again to cycle over options.")
      (license license:gpl3+)
      (home-page "https://gitlab.com/ideasman42/emacs-recomplete"))))

(define-public emacs-cycle-at-point
  (let ((commit "ea22b90f35f4cef73387047b3ef3fad83787d4e2")
        (revision "1"))
    (package
      (name "emacs-cycle-at-point")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/ideasman42/emacs-cycle-at-point")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "100aziv6wwrkalx07sy8za6kvnj30pknj1shbymspw13bpp7wqxj"))))
      (build-system emacs-build-system)
      (propagated-inputs (list emacs-recomplete))
      (synopsis "Cycle (rotate) the thing under the cursor")
      (description "Immediately cycle text at the cursor, without
prompting. Unlike most actions to select from a list cycle-at-point
replaces the text immediately, calling again to cycle over options.")
      (license license:gpl3+)
      (home-page "https://gitlab.com/ideasman42/emacs-cycle-at-point"))))
