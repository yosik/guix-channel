;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages web)
  #:use-module (guix packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages gnunet)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages search)
  #:use-module (gnu packages python)
  #:use-module (gnu packages bittorrent)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module ((guix licenses) #:prefix license:))

;; This packages are broken and outdated in main guix channel
;; And I am too lazy to send patches

(define-public libzim
  (package
    (name "libzim")
    (version "7.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/openzim/libzim")
                    (commit version)))
              (sha256
               (base32
                "0sh2lmc574kjjz3v2djaxfsf0sbsrdnj78cw444slpzqn40i91hz"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     ;; TODO: Find out why tests fail.
     '(#:tests? #f))
    (inputs
     (list icu4c
           xz
           util-linux
           xapian
           python
           `(,zstd "lib")))
    (native-inputs
     (list pkg-config googletest))
    (home-page "https://wiki.openzim.org/wiki/Main_Page")
    (synopsis "Reference implementation of the ZIM specification")
    (description "The openZIM project proposes offline storage solutions for
content coming from the Web.  The zimlib is the standard implementation of the
ZIM specification.  It is a library which implements the read and write method
for ZIM files.")
    (license license:gpl2)))

(define-public kiwix-lib
  (package
    (name "kiwix-lib")
    (version "10.0.0")
    (home-page "https://github.com/kiwix/kiwix-lib/")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url home-page)
                    (commit version)))
              (sha256
               (base32
                "1gkqwnq72dl5ky4r38ac242igrag9vw4c5b2qhkmqnbjpn13yg0n"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'fix-paths-and-includes
           (lambda* (#:key inputs #:allow-other-keys)
             (setenv "CPPFLAGS" (string-append "-I" (assoc-ref inputs "mustache")))
             (substitute* "src/aria2.cpp"
               (("ARIA2_CMD \"aria2c\"")
                (string-append "ARIA2_CMD \""
                               (assoc-ref inputs "aria2")
                               "/bin/aria2c\"")))
             #t)))))
    (inputs
     (list aria2
           curl
           icu4c
           libmicrohttpd
           libzim
           python
           pugixml
           xapian
           zlib
           `(,zstd "lib")))
    (native-inputs
     `(("mustache" ,(origin
                      (method git-fetch)
                      (uri (git-reference
                            (url "https://github.com/kainjow/Mustache")
                            ;; XXX: Readme says to use version 3.  Can we use 3.2.1?
                            (commit "v4.1")))
                      (file-name (git-file-name "mustache" "4.1"))
                      (sha256
                       (base32
                        "0r9rbk6v1wpld2ismfsk2lkhbyv3dkf0p03hkjivbj05qkfhvlbb"))))
       ("pkg-config" ,pkg-config)))
    (synopsis "Common code base for all Kiwix ports")
    (description "The Kiwix library provides the Kiwix software suite core.
It contains the code shared by all Kiwix ports.")
    (license license:gpl3)))

(define-public kiwix-tools
  (package
    (name "kiwix-tools")
    (version "3.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/kiwix/kiwix-tools")
             (commit version)))
       (sha256
        (base32
         "05h30amf90n8ii2qj9dhp2kriqk25y9mk9swf6nb0m532j87r22i"))
       (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (inputs
     (list curl
           icu4c
           kiwix-lib
           libzim
           libmicrohttpd
           pugixml
           xapian
           zlib
           `(,zstd "lib")))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (synopsis "Command line Kiwix tools: kiwix-serve, kiwix-manage, kiwix-read,
kiwix-search")
    (description
     "The Kiwix tools is a collection of @url{Kiwix, https://kiwix.org/} related
command line tools:

@itemize
@item @command{kiwix-manage}: Manage XML based library of ZIM files
@item @command{kiwix-read}: Read ZIM file content
@item @command{kiwix-search}: Fulltext search in ZIM files
@item @command{kiwix-serve}: HTTP daemon serving ZIM files
@end itemize")
    (license license:gpl3+)
    (home-page "https://github.com/kiwix/kiwix-tools")))
