;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages maths)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:))

;; TODO: Fix tests (they exist, but I can't run them)
(define-public yacas
  (package
    (name "yacas")
    (version "1.9.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/grzegorzmazur/yacas/")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "0dqgqvsb6ggr8jb3ngf0jwfkn6xwj2knhmvqyzx3amc74yd3ckqx"))
       (file-name (git-file-name name version))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DENABLE_CYACAS_GUI=OFF"
                           "-DENABLE_CYACAS_UNIT_TESTS=OFF"
                           "-DENABLE_CYACAS_BENCHMARKS=OFF")
       #:build-type "Release"
       #:tests? #f))
    (synopsis "Computer calculations made easy")
    (description
     "Yacas (Yet Another Computer Algebra System) is a small and highly flexible
general-purpose Computer Algebra System (CAS).  The syntax uses an infix-operator
grammar parser.  The distribution contains a small library of mathematical
functions, but its real strength is in the language in which you can easily
write your own symbolic manipulation algorithms.  The core engine supports
arbitrary precision arithmetic and is able to execute symbolic manipulations on
various mathematical objects by following user-defined rules.")
    (license license:lgpl2.1+)
    (home-page "https://yacas.org")))
