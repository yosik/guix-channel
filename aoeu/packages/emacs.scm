;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages emacs)
  #:use-module (gnu packages emacs)
  #:use-module (guix packages)
  #:use-module (guix git-download))

(define-public emacs-next-latest
  (let ((commit "3306e11107595524bdde90482023077b69e6c733")
        (revision "1"))
    (package
      (inherit emacs-next)
      (name "emacs-next-latest")
      (version (git-version "29.0.50" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/emacs.git/")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0bpidkqpmql8bipcf0xbmqcqby6j3vc3g5sxszskm213pafic96g")))))))
