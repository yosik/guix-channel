;;; Copyright 2021 Alexandru-Sergiu Marton <brown121407@posteo.ro>
;;; Copyright 2021 Mihail Iosilevitch <mihail.iosilevitch@yandex.ru>
;;;
;;; This file is part of guix-channel.
;;;
;;; guix-channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guix-channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guix-channel.  If not, see <http://www.gnu.org/licenses/>.

(define-module (aoeu packages raku)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system rakudo)
  #:use-module (gnu packages bdw-gc)
  #:use-module (gnu packages c)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module ((guix-121407 packages c) #:prefix 121407:)
  #:use-module ((guix-121407 build-system rakudo) #:prefix 121407:))

(define-public moarvm
  (package
    (name "moarvm")
    (version "2021.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://moarvm.org/releases/MoarVM-"
                           version ".tar.gz"))
       (sha256
        (base32
         "0xddjp21d44cjn30r570g01iv87n956b24ig9q7bwxwb1skqfd3z"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Delete libraries which we can provide through Guix.
           (delete-file-recursively "3rdparty/dyncall")
           (delete-file-recursively "3rdparty/libatomicops")
           (delete-file-recursively "3rdparty/libuv")
           (delete-file-recursively "3rdparty/libtommath")
           ;; Useful only for Microsoft Visual Studio.
           (delete-file-recursively "3rdparty/msinttypes")
           #t))))
    (build-system perl-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out        (assoc-ref outputs "out"))
                   (pkg-config (assoc-ref inputs "pkg-config")))
               (setenv "LDFLAGS"
                       (string-append "-Wl,-rpath=" out "/lib"))
               (invoke "perl" "Configure.pl"
                       (string-append "--prefix=" out)
                       (string-append "--pkgconfig=" pkg-config
                                      "/bin/pkg-config")
                       "--has-libtommath"
                       "--has-libatomic_ops"
                       "--has-libffi"
                       "--has-libuv"
                       "--has-dyncall")))))))
    (home-page "https://moarvm.org/")
    ;; These should be inputs but moar.h can't find them when building
    ;; rakudo
    (propagated-inputs
     `(("libatomic-ops" ,libatomic-ops)
       ("libffi" ,libffi)
       ("libtommath" ,libtommath)
       ("libuv" ,libuv)
       ("dyncall" ,121407:dyncall)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (synopsis "VM for NQP And Rakudo Perl 6")
    (description
     "Short for \"Metamodel On A Runtime\", MoarVM is a modern virtual
machine built for the Rakudo Perl 6 compiler and the NQP Compiler
Toolchain. Highlights include:

@itemize
@item Great Unicode support, with strings represented at grapheme level
@item Dynamic analysis of running code to identify hot functions and
loops, and perform a range of optimizations, including type
specialization and inlining
@item Support for threads, a range of concurrency control constructs,
and asynchronous sockets, timers, processes, and more
@item Generational, parallel, garbage collection
@item Support for numerous language features, including first class
functions, exceptions, continuations, runtime loading of code, big
integers and interfacing with native libraries.
@end itemize")
    (license (list license:artistic2.0  ; For MoarVM itself
                   license:expat        ; 3rdparty/freebsd,tinymt,cmp
                   license:public-domain)))) ; 3rdparty/sha1

(define-public nqp
  (package
    (name "nqp")
    (version "2021.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/Raku/nqp/releases/download/" version
             "/nqp-" version ".tar.gz"))
       (sha256
        (base32
         "0jab54inrzffzw1ba1drss1yr3k8wwqnbx6fgk2w28nfng0mn4s8"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           (delete-file-recursively "3rdparty/asm")
           (delete-file-recursively "3rdparty/jline")
           (delete-file-recursively "3rdparty/jna")
           #t))))
    (build-system perl-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-source-shebangs 'patch-more-shebangs
           (lambda _
             (substitute* '("tools/build/install-jvm-runner.pl.in"
                            "tools/build/gen-js-cross-runner.pl"
                            "tools/build/gen-js-runner.pl"
                            "tools/build/install-js-runner.pl"
                            "t/nqp/111-spawnprocasync.t"
                            "t/nqp/113-run-command.t")
               (("/bin/sh") (which "sh")))
             #t))
         (replace 'configure
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out  (assoc-ref outputs "out"))
                   (moar (assoc-ref inputs "moarvm")))
               (invoke "perl" "Configure.pl"
                       "--backends=moar"
                       (string-append "--with-moar=" moar "/bin/moar")
                       (string-append "--prefix=" out))))))))
    (inputs
     `(("moarvm" ,moarvm)))
    (home-page "https://github.com/Raku/nqp")
    (synopsis "Not Quite Perl")
    (description "This is \"Not Quite Perl\" -- a lightweight Perl
6-like environment for virtual machines. The key feature of NQP is that
it's designed to be a very small environment (as compared with, say,
perl6 or Rakudo) and is focused on being a high-level way to create
compilers and libraries for virtual machines like MoarVM, the JVM, and
others.

Unlike a full-fledged implementation of Perl 6, NQP strives to have as
small a runtime footprint as it can, while still providing a Perl 6
object model and regular expression engine for the virtual machine.")
    (license license:artistic2.0)))

(define-public rakudo
  (package
    (name "rakudo")
    (version "2021.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://rakudo.org/dl/rakudo/rakudo-"
                           version ".tar.gz"))
       (sha256
        (base32
         "1894r8m5r9pmdj8rgic43c9p16qmlygyfia2yzivbvr8fd9wfx5i"))))
    (build-system perl-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after 'patch-source-shebangs 'patch-more-shebangs
           (lambda _
             (substitute* '("tools/build/create-js-runner.pl"
                            "tools/build/create-jvm-runner.pl"
                            "src/core.c/Proc.pm6")
               (("/bin/sh") (which "sh")))
             #t))
         (add-after 'unpack 'remove-failing-test
           ;; One subtest fails for unknown reasons
           (lambda _
             (delete-file "t/09-moar/01-profilers.t")
             #t))
         (replace 'configure
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (nqp (assoc-ref inputs "nqp")))
               (invoke "perl" "./Configure.pl"
                       "--backend=moar"
                       (string-append "--with-nqp=" nqp "/bin/nqp")
                       "--prefix" out))))
         ;; This is the recommended tool for distro maintainers to
         ;; install perl6 modules systemwide. See:
         ;; https://github.com/ugexe/zef/issues/117
         (add-after 'install 'install-dist-tool
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (dest (string-append out "/share/perl6/tools")))
               (install-file "tools/install-dist.p6" dest)
               (substitute* (string-append dest "/install-dist.p6")
                 (("/usr/bin/env raku")
                  (string-append out "/bin/raku"))))
             #t)))))
    (inputs
     `(("moarvm" ,moarvm)
       ("nqp" ,nqp)
       ("openssl" ,openssl)))
    (home-page "https://rakudo.org/")
    (native-search-paths
     (list (search-path-specification
            (variable "PERL6LIB")
            (separator ",")
            (files '("share/perl6/lib"
                     "share/perl6/site/lib"
                     "share/perl6/vendor/lib")))))
    (synopsis "Raku Compiler")
    (description "Rakudo is a compiler that implements the Perl 6
specification and runs on top of several virtual machines.")
    (license license:artistic2.0)))

(define-public raku-tap-harness
  (package
    (name "raku-tap-harness")
    (version "0.2.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/raku/tap-harness6")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1w6yjawh6ald8m39ph6gx8v74rv5a09xf285p5gvqwjipya0gq59"))))
    (build-system 121407:rakudo-build-system)
    (arguments
     '(#:with-zef? #f
       #:with-prove6? #f))
    (home-page "https://github.com/perl6/tap-harness6/")
    (synopsis "TAP harness for perl6")
    (description "This module provides the @command{prove6} command
which runs a TAP based test suite and prints a report. The
@command{prove6} command is a minimal wrapper around an instance of this
module.")
    (license license:artistic2.0)))

(define-public zef
  (package
    (name "zef")
    (version "0.13.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ugexe/zef")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1gd10qb0j3zdf7gj4wcfid1q35izhvpfb2xrv6spwbmfh0q1ff6d"))))
    (build-system 121407:rakudo-build-system)
    (arguments
     '(#:with-zef? #f
       ;; Fails with strange error otherwise:
       #:with-prove6? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'check
           (lambda _
             (setenv "HOME" "/tmp")
             (invoke "raku" "-I." "bin/zef" "--debug"
                     "--tap-harness" "test" "."))))))
    (home-page "https://github.com/ugexe/zef")
    (synopsis "Raku Module Management")
    (description "Zef is a Raku package (module) manager. It can be used
to download and install Raku modules in your home directory or as a
system-wide module.")
    (license license:artistic2.0)))
